**CrystalCraftMC Discipline Policy**

*Updated 3/18/15 -- Rev. 7*

This is the official document describing when and how to take action
against players who

are breaking the server's rules. Whenever you are going to take action
against a player for

breaking a rule, refer to this guide to see what punishment needs to
given to that player.

Also, one of the **most** important things you need to do when punishing
a player is to tell

them WHAT it is exactly they did wrong. With most commands, you can add
a reason that is

displayed to the player, like with /ban, /kick, and as of recently,
/tempban. However, with /mute,

this is not possible. So please make sure you inform the player what
they did wrong BEFORE

taking action against them with a /mute. If you are banning or kicking,
just include a reason.

**Rule \#1: Griefing, raiding, and PvPing are allowed.**

*No guidelines needed*

**Rule \#2: No asking for items or services from staff.**

This is when someone asks to be given a rank, promotion, item(s), or
have staff run a

command for them immediately in the game, or they ask to do something to
become a staff

member. We do not hand out donations on any terms. This is a minor rule
to break, but

ultimately, it can get annoying after a while.

●

**Examples**:

○

\"can i be op\"

○

\"Could I please be teleported to another player?\"

●

**Penalties**:

1.

**First Offense**: Warning in chat

2.

**Second Offense**: *Final* warning and ten minute mute

■ **Example**: \"\[*Player*\]*, this is your final warning for asking
for items/services*

*from staff.*\" And then mute the player for ten minutes.

3.

**Third Offense**: Warning and seven day mute

 {#section-1 style="page-break-before:always; "}

**Rule \#3: No hacking or x-raying.**

There are three different types of violations to this rule, being
**Minor Hacking**, **Major**

**Hacking**, and **X-Raying**.

**Minor Hacking**

This would be considered speed hacks, wall-climbing hacks, attempts at
flying, fast

break / fast place, etc. Minor hacking is basically anything that
benefits the one player, but does

not greatly affect other players. If it is something that could damage
the server or affect the

balance of valuable items in the game, it would be considered Major
Hacking.

●

**Example**: xXHackingPlayerXx just is walking faster than should be
possible.

●

**Penalties**:

1.

**First Offense**: Warning to remove hacks and two week ban

2.

**Second Offense**: Warning and six month ban

3.

**Third Offense**: Full ban

**Major Hacking**

This would be creative mode hacks, anything resembling admin abilities,
and anything

similar. These are all punishable by immediate ban. There are no
questions asked. Only ban for

major hacking if you are CERTAIN that it is hacks.

●

**Example**: Yoshi\_Sama has the Admin rank.

●

**Penalties**:

○

**First Offense**: Instant FULL ban (and maybe freak out on Slack)

**X-Raying**

This would be considered any player attempting to see through blocks to
mine diamond

faster than should be possible. We currently are only using ourselves
and a basic block

obfuscation software as an x-ray prevention, so you have to be alert for
the signs of x-raying

and be *confident* when you issue your punishment. Don\'t let the hacker
talk you out of your

decision.

●

**Example**: xXHackingPlayerXx is mining in dark tunnels that turn in
EXTREMELY

random directions, and they also have a high ratio of stone and diamonds
in their

inventory.

*Penalties on next page*

 {#section-2 style="page-break-before:always; "}

●

**Penalties**:

1.

**First Offense**: \"Final\" warning to remove x-ray and six month ban

■ **Example**: \"*\[Player\], this is your only and final warning for
x-raying.*\" And

then ban the player for six months.

2.

**Second Offense**: Full ban

**Rule \#4: No spawn-killing.**

Spawn-killing is the deliberate act of killing a player within 800
blocks of spawn. It is not

classified as spawn-killing unless a player who is BEING FOLLOWED is
actually killed by their

assailant. If the person who is FOLLOWING without permission is killed,
it is NOT spawn-killing.

If a player followed another outside of spawn, it does NOT count as
spawn-killing and no action

is to be taken. There are numerous exits out of spawn, and we have
deliberately made this to

the advantage of the player.

●

**Example**: xXRuleBreakerXx killed xXNewbiePlayerXx within 600 blocks
from spawn.

●

**Penalties**:

1.

**First Offense**: Warning not to spawn-kill (provide definition of
spawn-killing if

needed) and kick

2.

**Second Offense**: Warning and one hour ban

3.

**Third Offense**: \"Final\" warning and one month ban

■ **Example**: \"*\[Player\], this is your final warning for
spawn-killing.*\"

4.

**Fourth Offense**: Full ban

**Rule \#5: Spamming the chat is not permitted.**

This would be considered any player is typing the same message with or
without

different characters in the wording multiple times in chat or someone
who is otherwise being

annoying with repetitive messages in chat.

●

**Examples**:

○

xXSpammingPlayerXx: \"OMG STOP\" (repeated numerous times)

○

xXSpammingPlayerXx: \"hi\" (repeated numerous times)

●

**Penalties**:

1.

**First Offense**: Warning to stop spamming and kick

2.

**Second Offense**: Warning and one hour mute

3.

**Third Offense**: \"Final\" warning and seven day mute

■ **Example**: \"\[*Player*\]*, this is your final warning for
spamming.*\"

4.

**Fourth Offense**: Permanent mute

 {#section-3 style="page-break-before:always; "}

**Rule \#6: No advertising other servers.**

This would be considered any player who is either deliberately
advertising another

server IP / website or spamming another server name in the OPEN CHAT. If
a player is

spamming other random users privately with a server IP, that is breaking
this rule; however, if it

is between two people who are known to be friends or they have been
talking for a while, it is

okay for them to privately share an IP. Like with hacking, there are two
different types of

advertising: **Intentional** and **Conversational**.

**Intentional Advertising**

●

**Example**: xXAdvertisingPlayerXx: \"JOIN ICMC.FARTED.NET\"

●

**Penalties**:

1.

**First Offense**: Full ban

**Conversational Advertising**

●

**Example**: xXAdvertisingPlayerXx: \"Want to go hop on MedievalCraft,
anyone? IP is

play.niruvankrules.com.\"

●

**Penalties**:

1.

**First Offense**: Warning and kick

2.

**Second Offense**: "Final" warning and seven day mute

○

**Example**: \"\[*Player*\]*, this is your final warning for advertising
another*

*server.*\"

3.

**Third Offense**: Permanent mute

**Rule \#7: No causing drama.**

Causing drama is defined by a player *drawing attention to something
particularly*

*negative* or *being a negative influence* themselves on other players
to the point where it is

*taking away from the server experience*.

●

**Examples**:

○

xXDramaQueenXx: \"HAH! I scaaaaaamed you!\"

○

xXDramaQueenXx: "grow up and get a pair"

○

xXDramaQueenXx: "stfu / gtfo (etc)." \[repeatedly, such as continual
harassment\]

○

(*Any combination of the above things towards a single person or group*)

*Penalties on the next page*

 {#section-4 style="page-break-before:always; "}

●

**Penalties**:

1.

**First Offense**: Warning to stop causing drama and kick

2.

**Second Offense**: Warning and one hour mute

3.

**Third Offense**: Warning and two day mute

4.

**Fourth Offense**: \"Final\" warning and seven day ban

■ **Example**: \"\[*Player*\]*, this is your final warning for causing
unnecessary*

*drama.*\"

5.

**Fifth Offense**: Warning and one month ban

6.

**Sixth Offense**: Permanent mute

**Rule \#8: No abusing any glitches within the game.**

Abusing glitches in Minecraft could mean taking advantages of actual
bugs in the game

itself or trying to bypass intentional limitations set by the staff in
plugins we use or other means

of limitations. Players should know that any glitches or exploits should
be reported immediately

to staff, and if they continually abuse these glitches without notifying
us, it will result in a

punishment as defined below.

●

**Examples**:

○

xXAbusivePlayerXx: \*uses /ejump to get into the City of Occurrences\*

○

xXAbusivePlayerXx: \*uses commands / other means to get under spawn\*

●

**Penalties**:

○

**First Offense**: "Final" warning to reveal how they did it

○

**Second Offense**: If they don't comply...

■ *If command-related*: Take away ALL donor commands for a week

■ *If game-related*: Three day ban

○

**Third Offense**: If they don't comply...

■ *If command-related*: Permanent revocation of ALL commands

■ *If game-related*: One month ban

○

**Fourth Offense**:

■ *If game-related*: Six month ban

○

**Fifth Offense**:

■ *If game-related*: Permanent ban

■

 {#section-5 style="page-break-before:always; "}

**Rule \#9: No excessive profanity or obscenity.**

The thing to keep in mind is that *swearing IS permitted*, but if a
player is constantly

swearing to the point where it's just become a nuisance, action needs to
be taken. Swearing by

itself is something we cannot not action against and will not support
for a punishment, but if a

player is being excessively abusive towards others to the point it
disrupts the experience for

others, we need to take action.

In terms of obscenity, anything sexual, racial, or otherwise personally
offensive will

probably fall into this category.

●

**Examples**:

○

xXProfanePlayerXx: "SHUT THE FUCK UP BITCH."

■ Or anything similar where obscene words are being used for the point
of

saying obscene words or to continually degrade others

○

xXObscenePlayerXx: "Heil Hitler!"

○

xXObscenePlayerXx: "lets talk about sex"

●

**Penalties**:

1.

**First Offense**:

■ *If profanity*: Kick for excessive profanity

■ *If slightly sexual*: Kick for inappropriate topic

■ *If overly sexual*: Permanent mute

■ *If racial*: Permanent ban

2.

**Second Offense**:

■ *If profanity*: One week mute

■ *If slightly sexual*: One month mute

3.

**Third Offense**:

■ *If profanity*: One month mute

■ *If slightly sexual*: Permanent mute

4.

**Fourth Offense**:

■ *If profanity*: Permanent mute

 {#section-6 style="page-break-before:always; "}

**Rule \#10: Always be respectful of server staff.**

Being staff isn't fun and games sometimes. Sometimes new and old players
alike break

the rules and we need to take action against them regardless. For
obvious reasons, this may

make people unhappy and irate. As server staff, it is important to
realize that we need to keep

calm when faced with senseless accusations, and refer to this rule for a
situation where a player

is being overly irate about how a punishment that may have been given.

These set of rules can most certainly apply to TeamSpeak3 voice chat as
well.

●

**Examples**:

○

xXAngryManXx: "WTF MODERATOR THATS FUCKING STUPID"

○

xXSmartAssXx: "Do you know the rules, moderator? Do your job!"

○

xXSmartAssXx: "That moderator is a retard. Fucking dumbass."

●

**Penalties**:

○

**First Offense**: Kick saying to be respectful of staff decisions

○

**Second Offense**: One hour mute

○

**Third Offense**: One week mute

○

**Fourth Offense**: Permanent mute and refer to jflory7

**Rule \#11: Punishment evasion is prohibited.**

Using alternative accounts to evade any sort of punishment, whether it
is a ban or a

mute, is not allowed and will not be tolerated. The only evidence
sufficient enough to call it

evasion is either a matching IP to the punished account or the user
claiming to be the punished

account.

If possible, please mirror the initial punishment across all known
alternative accounts

beforehand to help minimize the problem in the future.

●

**Examples**:

○

NotVectiic: "i am vectiic can i please have a new chance"

○

xXEvaderXx: "I know a lot about this server. ;)" \[IP matches punished
user\]

●

**Penalties**:

○

**First Offense**: Warning and mirror punishment on main account on the

alternative account

○

**Second Offense**: Double the initial punishment on ALL known accounts

○

**Third Offense**: Permanent account and IP ban

 {#section-7 style="page-break-before:always; "}

**Compound Punishment System**

In the event that a player was punished before for breaking one rule,
and then broke

another, ***technically*** this could mean they would just get off on a
warning; however, that is not

what we want to do.

If a user has been punished before, then they should very well know the
rules and be

familiar with what is allowed and what is not.

However, the one issue with this is that it is very subjective. In the
event that a player

has received ONLY a warning for another action, and then they break a
rule for a different

action, then they should only receive a "First Offense" punishment.

Then there are cases where users have received definite punishment for
actions, such

as a two hour mute for causing drama or something similar. Let's say
that user then insults a

staff decision and criticizes their ability to moderate. They should not
just receive the normal

kick, but the punishment should be escalated on what seems to be the
next tier of punishment.

Also, there are other types of actions that should be considered "high
level" actions for

players who already have a track record. For example, let's take a
player who was muted for

two days once before for breaking a rule. Later, we catch the same
player x-raying to find ores.

He shouldn't just receive the standard two week ban for x-raying, but
rather, he should go

straight to a full ban.

Again, these actions are extremely subjective and asking for advice from
other staff

members is not a bad idea! Please consult other staff members in Slack
if you are dealing with

a troublesome player who has broken rules before and you are unsure of
which punishment is

suitable for the player.

*If you have any ideas for revisions, please mention them to Justin!*
