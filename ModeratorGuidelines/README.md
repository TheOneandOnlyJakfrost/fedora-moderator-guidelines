# ModeratorGuidelines

Moderator Guidelines for the Fedora Forums

### Introduction

The Fedora community is a diverse and growing community. The Fedora community has always been a welcoming place for it's members to participate in all things Fedora, and beyond. It is important to maintain that level of compfort within the community since, at the least, it brings the best to the forefront from all of us. This is an effort to help with the more difficult moderation tasks that can present themselves in such a diverse community. It has become apparent that a set of guidelines, or principles, distilled from the Fedora Core values, and filtered through the Fedora Code of Conduct would help to provide a consistency of experience for the community members, and help maintain that community involvement comfort level.

### Note about files here

Since this is an idea in the inception stage, there are files found here which were included as examples  of in use moderator guidelines on other forums. As such their content is largely focused around the specific community they were intended for. However, there are similarities in any online forum based community which can be readily transposed across all of them. The files of note are as follows ...

- Discipline-Policy.pdf/.odg/.html/.md - this file is a Discipline Policy from the CrystalCraftMC community forum. It is intended as a document to draw practical examples from.

- spigot-rules.md -  this file is also another example document of moderator guidelines which can be used as a resource.


